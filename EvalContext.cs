using System;
using System.Collections.Generic;

namespace Cigara
{
    public class VarContext
    {
        private Dictionary<string, NodeEvalResult> vars;
        private bool cycle_context;

        public VarContext()
        {
            vars = new Dictionary<string, NodeEvalResult>();
            cycle_context = false;
        }

        public void AddVariable(string name, NodeEvalResult value)
        {
            if (vars.ContainsKey(name) == false)
            {
                vars.Add(name, value);
            } else
            {
                throw new InterpreterException(String.Format("Variable with name {0} is defined!", name));
            }
        }

        public NodeEvalResult GetVariable(string name)
        {
            if (vars.ContainsKey(name))
            {
                return vars[name];
            }
            else
            {
                throw new InterpreterException(String.Format("Variable with name {0} is undefined!", name));
            }
        }

        public bool VariableDefined(string name)
        {
            return vars.ContainsKey(name);
        }

        public void SetVariable(string name, NodeEvalResult value)
        {
            if (vars.ContainsKey(name))
            {
                vars[name] = value;
            }
            else
            {
                throw new InterpreterException(String.Format("Variable with name {0} is undefined!", name));
            }
        }

        public void SetCycleContext(bool v)
        {
            cycle_context = v;
        }

        public bool isCycleContext()
        {
            return cycle_context;
        }
    }

    public enum EvalState { CONTINUE, RETURN, BREAK };

    public class EvalContext
    {
        private List<VarContext> varContexts;
        private VarContext currentContext;
        private NodeEvalResult retval;
        private EvalState evalState;

        public EvalContext()
        {
            varContexts = new List<VarContext>();
            retval = new NodeEvalResult(0);
            evalState = EvalState.CONTINUE;
        }

        public void PushVarContext()
        {
            VarContext ctx = new VarContext();
            currentContext = ctx;
            varContexts.Add(ctx);
        }

        public void PopVarContext()
        {
            if (varContexts.Count > 0)
            {
                varContexts.RemoveAt(varContexts.Count - 1);
                if (varContexts.Count > 0)
                {
                    currentContext = varContexts[varContexts.Count - 1];
                }
            }
        }

        public void SetRetValue(NodeEvalResult value)
        {
            retval = value;
        }

        public NodeEvalResult GetRetValue()
        {
            return retval;
        }

        public void SetEvalState(EvalState state)
        {
            evalState = state;
        }

        public EvalState GetEvalState()
        {
            return evalState;
        }

        public void AddVariable(string name, NodeEvalResult value)
        {
            currentContext.AddVariable(name, value);
        }

        public NodeEvalResult GetVariable(string name)
        {
            for (int i = varContexts.Count - 1; i >= 0; --i)
            {
                if (varContexts[i].VariableDefined(name))
                {
                    return varContexts[i].GetVariable(name);
                }
            }
            return new NodeEvalResult();
        }

        public bool VariableDefined(string name)
        {
            for (int i = varContexts.Count - 1; i >= 0; --i)
            {
                if (varContexts[i].VariableDefined(name))
                {
                    return true;
                }
            }
            return false;
        }

        public void SetVariable(string name, NodeEvalResult value)
        {
            for (int i = varContexts.Count - 1; i >= 0; --i)
            {
                if (varContexts[i].VariableDefined(name))
                {
                    varContexts[i].SetVariable(name, value);
                    break;
                }
            }
        }

        public void SetCycleContext(bool v)
        {
            currentContext.SetCycleContext(v);
        }

        public bool isCycleContext()
        {
            for (int i = varContexts.Count - 1; i >= 0; --i)
            {
                if (varContexts[i].isCycleContext())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
