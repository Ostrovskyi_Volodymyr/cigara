%{

%}

%namespace Cigara
%parsertype CigaraParser
%visibility internal
%output=CigaraParser.cs

%union {
    public NStmtsBlock stmtsBlock;
    public NStatement stmt;
    public NExpression expr;
    public List<NIdentifier> ident_list;
    public List<NExpression> expr_list;
    public NIdentifier ident;
    public string str_val;
    public int int_val;
    public int token;
    public char char_val;
    public string string_val;
}

%token<str_val> TIDENT
%token<int_val> TINTEGER
%token<char_val> TCHAR
%token<string_val> TSTRING
%token<token> TPLUS TMINUS TMUL TDIV TLESS
%token<token> TGREAT TEQUAL TNEQUAL
%token TKEYBEGIN TKEYEND TKEYFUNC TKEYRETURN TKEYBREAK
%token TKEYTRUE TKEYFALSE TKEYIF TKEYELSE TKEYWHILE TKEYVAR
%token TEND TLPARENT 
%token TRPARENT TSEMICOLON TCOLON TLBRACKET
%token TRBRACKET TCOMMA TASSIGN TDOLLAR

%type<stmtsBlock> program stmts stmts_block
%type<stmt> stmt func_decl var_decl return_stmt if_stmt while_stmt break_stmt
%type<ident_list> param_list params
%type<expr> expr var_expr call_expr array_expr string_expr numeric boolean char ident
%type<expr> var_change_expr index_expr array_change_expr binary_op_expr
%type<expr_list> arg_list args array_elems
%type<token> binary_op
%type<ident> ident

%right TASSIGN
%left TLESS TGREAT
%left TEQUAL TNEQUAL
%left TPLUS TMINUS
%left TMUL TDIV
%right UMINUS

%start program

%%

program:    stmts { Interpreter.program = $1; }
        |   error { YYABORT; }
;

stmts:      stmt { $$ = new NStmtsBlock(); $$.stmts.Add($1); }
        |   stmts stmt { $1.stmts.Add($2); }
;

stmt:       func_decl
        |   var_decl
        |   expr TSEMICOLON { $$ = new NStatementExpr($1); }
        |   return_stmt TSEMICOLON
        |   if_stmt
        |   while_stmt
        |   break_stmt TSEMICOLON
;

func_decl:  TKEYFUNC ident param_list stmts_block
            { $$ = new NFuncDecl($2, $3, $4); Interpreter.AddFunctionDecl($$ as NFuncDecl); }
;

param_list: TLPARENT params TRPARENT { $$ = $2; }
        |   TLPARENT TRPARENT { $$ = new List<NIdentifier>(); }
;

params:     ident { $$ = new List<NIdentifier>(); $$.Add($1); }
        |   params TCOMMA ident { $1.Add($3); }
;

stmts_block:
            TKEYBEGIN stmts TKEYEND { $$ = $2; }
        |   TKEYBEGIN TKEYEND { $$ = new NStmtsBlock(); }
;

var_decl:   TKEYVAR ident TASSIGN expr TSEMICOLON { $$ = new NVarDecl($2, $4); }
;

return_stmt:
            TKEYRETURN expr { $$ = new NReturnStmt($2); }
        |   TKEYRETURN { $$ = new NReturnStmt(); }
;

if_stmt:    TKEYIF TLPARENT expr TRPARENT stmts_block { $$ = new NIfStmt($3, $5); }
        |   TKEYIF TLPARENT expr TRPARENT stmts_block TKEYELSE stmts_block
            { $$ = new NIfStmt($3, $5, $7); }
;

while_stmt:
            TKEYWHILE TLPARENT expr TRPARENT stmts_block
            { $$ = new NWhileStmt($3, $5); }
;

break_stmt:      TKEYBREAK { $$ = new NBreakStmt(); }
;

expr:       numeric
        |   boolean
        |   char
        |   array_expr
        |   string_expr
        |   call_expr
        |   var_expr
        |   var_change_expr
        |   index_expr
        |   array_change_expr
        |   binary_op_expr
        |   TLPARENT expr TRPARENT { $$ = $2; }
;

array_expr:
            TLBRACKET array_elems TRBRACKET { $$ = new NArray($2); }
        |   TLBRACKET TRBRACKET { $$ = new NArray(); }
;

array_elems:
            expr { $$ = new List<NExpression>(); $$.Add($1); }
        |   array_elems TCOMMA expr { $1.Add($3); }
;

string_expr:
            TSTRING { $$ = new NString($1); }
;

call_expr:  ident arg_list { $$ = new NCallExpr($1, $2); }
;

arg_list:
            TLPARENT args TRPARENT { $$ = $2; }
        |   TLPARENT TRPARENT { $$ = new List<NExpression>(); }
;

args:
            expr { $$ = new List<NExpression>(); $$.Add($1); }
        |   args TCOMMA expr { $1.Add($3); }
;

var_expr:   ident { $$ = new NVarExpr($1); }
;

var_change_expr:
            var_expr TASSIGN expr { $$ = new NVarChangeExpr($1 as NVarExpr, $3); }
;

index_expr: expr TLBRACKET expr TRBRACKET { $$ = new NIndexExpr($1, $3); }
;

array_change_expr:
            index_expr TASSIGN expr { $$ = new NArrayChangeExpr($1 as NIndexExpr, $3); }
;

binary_op_expr:
            expr binary_op expr { $$ = new NBinaryOpExpr($1, $2, $3); }
;

binary_op:  TPLUS
        |   TMINUS
        |   TMUL
        |   TDIV
        |   TLESS
        |   TGREAT
        |   TEQUAL
        |   TNEQUAL
;

numeric:    TINTEGER { $$ = new NInteger($1); }
;

boolean:    TKEYTRUE { $$ = new NBoolean(true); }
        |   TKEYFALSE { $$ = new NBoolean(false); }
;

char:       TCHAR { $$ = new NChar($1); }
;

ident:      TIDENT { $$ = new NIdentifier($1); }
;

%%

public CigaraParser(ScanBase scn) : base(scn) { }
