%{

private System.Collections.Generic.Dictionary<string, int> keywords = 
	new System.Collections.Generic.Dictionary<string, int>() {
	{"begin", (int)Tokens.TKEYBEGIN},
	{"end", (int)Tokens.TKEYEND},
	{"func", (int)Tokens.TKEYFUNC},
	{"return", (int)Tokens.TKEYRETURN},
	{"True", (int)Tokens.TKEYTRUE},
	{"False", (int)Tokens.TKEYFALSE},
	{"if", (int)Tokens.TKEYIF},
	{"else", (int)Tokens.TKEYELSE},
	{"while", (int)Tokens.TKEYWHILE},
	{"var", (int)Tokens.TKEYVAR},
	{"break", (int)Tokens.TKEYBREAK},
};

private System.Collections.Generic.Dictionary<string, string> echars =
	new System.Collections.Generic.Dictionary<string, string>() {
	{"\\n", "\n"},
	{"\\t", "\t"},
	{"\\\\", "\\"},
};

private string Unescape(string s)
{
	foreach (var (str, symb) in echars)
	{
		s = s.Replace(str, symb);
	}
	return s;
}

public override void yyerror( string format, params object[] args )
{
	Console.WriteLine(String.Format("Line {0}: " + format, tokLin, args));
}

%}

%namespace Cigara
%option nofiles
%visibility internal

%%

[ \n\r\t]			;
"#".*\n				;
[a-zA-Z_][a-zA-Z0-9_]*  {
							yylval.str_val = yytext;
							if (keywords.ContainsKey(yytext)) {
								return keywords[yytext];
							}
							return (int) Tokens.TIDENT;
						}
[0-9]+					{ yylval.int_val = Int32.Parse(yytext); return (int)Tokens.TINTEGER; }
\'(\\.|[^'"\\]){1}\'	{ yylval.char_val = Unescape(yytext.Trim('\''))[0]; return (int)Tokens.TCHAR; }
\"(\\.|[^"\\])*\"		{ yylval.string_val = Unescape(yytext.Trim('\"')); return (int)Tokens.TSTRING; }
"+"						{ yylval.token = (int)Tokens.TPLUS; return yylval.token; }
"-"						{ yylval.token = (int)Tokens.TMINUS; return yylval.token; }
"*"						{ yylval.token = (int)Tokens.TMUL; return yylval.token; }
"/"						{ yylval.token = (int)Tokens.TDIV; return yylval.token; }
"<"						{ yylval.token = (int)Tokens.TLESS; return yylval.token; }
">"						{ yylval.token = (int)Tokens.TGREAT; return yylval.token; }
"=="					{ yylval.token = (int)Tokens.TEQUAL; return yylval.token; }
"!="					{ yylval.token = (int)Tokens.TNEQUAL; return yylval.token; }
"("						{ return (int)Tokens.TLPARENT; }
")"						{ return (int)Tokens.TRPARENT; }
";"						{ return (int)Tokens.TSEMICOLON; }
":"						{ return (int)Tokens.TCOLON; }
"["						{ return (int)Tokens.TLBRACKET; }
"]"						{ return (int)Tokens.TRBRACKET; }
","						{ return (int)Tokens.TCOMMA; }
"="						{ return (int)Tokens.TASSIGN; }
"$"						{ return (int)Tokens.TDOLLAR; }
.						{ Console.WriteLine("Unknown token!"); }

%%
