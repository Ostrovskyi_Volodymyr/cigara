﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Cigara
{
    class Program
    {

        static NodeEvalResult PrintFunc(List<NodeEvalResult> args)
        {
            foreach (var arg in args)
            {
                if (arg.type == NodeEvalResultType.INTEGER)
                {
                    Console.Write("{0}", arg.int_value);
                } else if (arg.type == NodeEvalResultType.BOOLEAN)
                {
                    Console.Write("{0}", arg.bool_value);
                } else if (arg.type == NodeEvalResultType.CHAR)
                {
                    Console.Write("{0}", arg.char_value);
                } else if (arg.type == NodeEvalResultType.STRING)
                {
                    Console.Write("{0}", arg.string_value);
                } else if (arg.type == NodeEvalResultType.ARRAY)
                {
                    Console.Write("[");
                    foreach (var elem in arg.array_value)
                    {
                        PrintFunc(new List<NodeEvalResult> { elem });
                        Console.Write(", ");
                    }
                    Console.Write("]");
                }
            }
            return new NodeEvalResult(0);
        }

        static NodeEvalResult PrintlnFunc(List<NodeEvalResult> args)
        {
            PrintFunc(args);
            Console.WriteLine();
            return new NodeEvalResult(0);
        }

        static NodeEvalResult LenFunc(List<NodeEvalResult> args)
        {
            if (args.Count > 0)
            {
                if (args[0].type == NodeEvalResultType.ARRAY)
                {
                    return new NodeEvalResult(args[0].array_value.Count);
                } else if (args[0].type == NodeEvalResultType.STRING)
                {
                    return new NodeEvalResult(args[0].string_value.Length);
                }
            }
            return new NodeEvalResult(0);
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Usage: Cigara.exe <filename.cg>");
                return;
            }

            var content = File.ReadAllText(args[0]);

            Scanner scn = new Scanner();
            scn.SetSource(content, 0);
            CigaraParser parser = new CigaraParser(scn);
            //parser.Trace = true;

            var extfunc = new Dictionary<string, Interpreter.ExternFunction>()
            {
                {"print", PrintFunc},
                {"println", PrintlnFunc},
                {"len", LenFunc},
            };

            Interpreter.Init();
            foreach (var (name, func) in extfunc)
            {
                Interpreter.AddExternFunctionDecl(name, func);
            }
            if (parser.Parse())
            {
                Console.WriteLine("===== AST =====");
                Interpreter.PrintAST();
                try
                {
                    Console.WriteLine("===== RUN =====");
                    Interpreter.Run();
                }
                catch (InterpreterException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
