using System.Collections.Generic;

namespace Cigara
{
    public enum NodeEvalResultType
    {
        INTEGER,
        BOOLEAN,
        CHAR,
        ARRAY,
        STRING
    };

    public class NodeEvalResult
    {
        public NodeEvalResultType type;
        public int int_value;
        public bool bool_value;
        public char char_value;
        public List<NodeEvalResult> array_value;
        public string string_value;

        public NodeEvalResult() { }
        public NodeEvalResult(NodeEvalResultType t)
        {
            type = t;
        }

        public NodeEvalResult(int int_value)
        {
            type = NodeEvalResultType.INTEGER;
            this.int_value = int_value;
        }

        public NodeEvalResult(bool bool_value)
        {
            type = NodeEvalResultType.BOOLEAN;
            this.bool_value = bool_value;
        }

        public NodeEvalResult(char char_value)
        {
            type = NodeEvalResultType.CHAR;
            this.char_value = char_value;
        }
        public NodeEvalResult(List<NodeEvalResult> array_value)
        {
            type = NodeEvalResultType.ARRAY;
            this.array_value = array_value;
        }

        public NodeEvalResult(string string_value)
        {
            type = NodeEvalResultType.STRING;
            this.string_value = new string(string_value);
        }

        public bool toBoolean()
        {
            if (type == NodeEvalResultType.INTEGER)
            {
                return int_value != 0;
            }
            else if (type == NodeEvalResultType.BOOLEAN)
            {
                return bool_value;
            } else if (type == NodeEvalResultType.CHAR)
            {
                return true;
            }
            return false;
        }

        public static NodeEvalResult operator <(NodeEvalResult r1, NodeEvalResult r2)
        {
            NodeEvalResult r = new NodeEvalResult(false);
            if (r1.type == r2.type)
            {
                if (r1.type == NodeEvalResultType.INTEGER)
                {
                    r.bool_value = r1.int_value < r2.int_value;
                } else if (r1.type == NodeEvalResultType.CHAR)
                {
                    r.bool_value = r1.char_value < r2.char_value;
                }
            }
            return r;
        }

        public static NodeEvalResult operator >(NodeEvalResult r1, NodeEvalResult r2)
        {
            NodeEvalResult r = new NodeEvalResult(false);
            if (r1.type == r2.type)
            {
                if (r1.type == NodeEvalResultType.INTEGER)
                {
                    r.bool_value = r1.int_value > r2.int_value;
                }
                else if (r1.type == NodeEvalResultType.CHAR)
                {
                    r.bool_value = r1.char_value > r2.char_value;
                }
            }
            return r;
        }

        public static NodeEvalResult operator ==(NodeEvalResult r1, NodeEvalResult r2)
        {
            NodeEvalResult r = new NodeEvalResult(false);
            if (r1.type == r2.type)
            {
                if (r1.type == NodeEvalResultType.INTEGER)
                {
                    r.bool_value = r1.int_value == r2.int_value;
                }
                else if (r1.type == NodeEvalResultType.CHAR)
                {
                    r.bool_value = r1.char_value == r2.char_value;
                }
            }
            return r;
        }

        public static NodeEvalResult operator !=(NodeEvalResult r1, NodeEvalResult r2)
        {
            NodeEvalResult r = new NodeEvalResult(false);
            if (r1.type == r2.type)
            {
                if (r1.type == NodeEvalResultType.INTEGER)
                {
                    r.bool_value = r1.int_value != r2.int_value;
                }
                else if (r1.type == NodeEvalResultType.CHAR)
                {
                    r.bool_value = r1.char_value != r2.char_value;
                }
            }
            return r;
        }
    }
}
