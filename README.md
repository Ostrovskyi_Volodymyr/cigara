# Cigara: Interpreted programming language ##

## Type system
Several data types are currently supported:  
 - Integer (10, 9, 5, etc.)  
 - Boolean (True or False)  
 - Char ('c', 'b', etc.)  
 - Array (data in brackets [0, 2 3])  
 - String ("Hello", "A", etc.)
___
## Syntax
### Var declaration
```
var <name> = <value>;

var i = 0;
var arr = [2, 4, 8];
```
### Function declaration
```
func <name>(<params>) begin
	<code>
end

func f1(a, b, c) begin
	return a + b + c;
end
```
### Function call
```
<name>(<args>);

f1(1, 2, 3);
```
### While statement
```
while (<cond>) begin
	<code>
end

while (True) begin
	println();
end
```
### If statement
```
if (<cond>) begin
	<code>
end else begin
	<code>
end

if (1 > 0) begin
	println(True);
end
```
### Comments
```
# Comment line
```
___
## Arithmetic operators
### Integer
For Integers +, -, *, / returns Integer.
### Array
The plus operator returns an array in cases:
```
9 + [1, 2, 3] => [9, 1, 2, 3]
[1, 2, 3] + 9 => [1, 2, 3, 9]
[1, 2] + [3, 4] => [1, 2, [3, 4]]
```
___
## Logical operators
For Integers >, <, ==, != returns Boolean:
```
5 > 6 == True
```
___
## Index operator
For Arrays index operator returns value by index:
```
arr = [10, 9, 8];
arr[0]; # 10
arr[2]; # 8
```
___
## Examples
### Get minimal element
```
func get_min(arr) begin
	var min = arr[0];
	var i = 0;

	while (i < len(arr)) begin
		if (min > arr[i]) begin
			min = arr[i];
		end
		i = i + 1;
	end
	return min;
end
```
### Array sort
```
func sort(arr) begin
	var i = 0;
	var size = len(arr);

	while (i < size) begin
		var j = 0;
		while (j < size - 1) begin
			if (arr[j] > arr[j + 1]) begin
				var t = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = t;
			end
			j = j + 1;
		end
		i = i + 1;
	end
end
```
### Merge two arrays
```
func merge(arr1, arr2) begin
	var i = 0;
	var res_arr = [];
	var size1 = len(arr1);
	var size2 = len(arr2);

	while (i < size1 + size2) begin
		if (i < size1) begin
			res_arr = res_arr + arr1[i];
		end else begin
			res_arr = res_arr + arr2[i - size1];
		end
		i = i + 1;
	end
	return res_arr;
end
```
