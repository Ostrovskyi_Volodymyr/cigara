using System;
using System.Collections.Generic;

namespace Cigara
{
    [Serializable]
    public class InterpreterException : Exception
    {
        public InterpreterException() { }

        public InterpreterException(string message)
            : base(message) { }

        public InterpreterException(string message, Exception inner)
            : base(message, inner) { }
    }

    public class Interpreter
    {
        public static NStmtsBlock program;
        private static List<EvalContext> evalContexts;
        private static EvalContext currentContext;
        private static EvalContext globalContext;
        private static Dictionary<string, NFuncDecl> lookupTable;

        public delegate NodeEvalResult ExternFunction(List<NodeEvalResult> args);

        private static Dictionary<string, ExternFunction> externLookupTable;

        public static void PrintAST()
        {
            Console.WriteLine(program);
        }

        public static void Init()
        {
            lookupTable = new Dictionary<string, NFuncDecl>();
            externLookupTable = new Dictionary<string, ExternFunction>();
        }

        public static void Run()
        {
            evalContexts = new List<EvalContext>();

            PushEvalContext();
            globalContext = currentContext;
            PushVarContext();
            program.Evaluate();
            PopVarContext();
            PopEvalContext();
        }

        public static void AddFunctionDecl(NFuncDecl funcdecl)
        {
            if (lookupTable.ContainsKey(funcdecl.ident.name) == false)
            {
                lookupTable.Add(funcdecl.ident.name, funcdecl);
            } else
            {
                throw new InterpreterException(String.Format("Function with name {0} is defined!", funcdecl.ident.name));
            }
        }

        public static NFuncDecl GetFunctionDecl(string name)
        {
            if (lookupTable.ContainsKey(name)) {
                return lookupTable[name];
            }
            return null;
        }

        public static void AddExternFunctionDecl(string name, ExternFunction funcdecl)
        {
            if (externLookupTable.ContainsKey(name) == false)
            {
                externLookupTable.Add(name, funcdecl);
            }
            else
            {
                throw new InterpreterException(String.Format("Extern function with name {0} is defined!", name));
            }
        }

        public static ExternFunction GetExternFunctionDecl(string name)
        {
            if (externLookupTable.ContainsKey(name))
            {
                return externLookupTable[name];
            }
            return null;
        }

        public static void PushEvalContext()
        {
            EvalContext ctx = new EvalContext();
            currentContext = ctx;
            evalContexts.Add(ctx);
        }

        public static void PopEvalContext()
        {
            if (evalContexts.Count > 0)
            {
                evalContexts.RemoveAt(evalContexts.Count - 1);
                if (evalContexts.Count > 0)
                {
                    currentContext = evalContexts[evalContexts.Count - 1];
                }
            }
        }

        public static void PushVarContext()
        {
            currentContext.PushVarContext();
        }

        public static void PopVarContext()
        {
            currentContext.PopVarContext();
        }

        public static void AddVariable(string name, NodeEvalResult value)
        {
            currentContext.AddVariable(name, value);
        }

        public static NodeEvalResult GetVariable(string name)
        {
            if (currentContext.VariableDefined(name))
            {
                return currentContext.GetVariable(name);
            } else if (globalContext.VariableDefined(name))
            {
                return globalContext.GetVariable(name);
            }
            throw new InterpreterException(String.Format("Variable with name {0} is undefined!", name));
        }

        public static void SetRetValue(NodeEvalResult value)
        {
            currentContext.SetRetValue(value);
        }

        public static NodeEvalResult GetRetValue()
        {
            return currentContext.GetRetValue();
        }

        public static void SetEvalState(EvalState state)
        {
            currentContext.SetEvalState(state);
        }

        public static EvalState GetEvalState()
        {
            return currentContext.GetEvalState();
        }

        public static void SetVariable(string name, NodeEvalResult value)
        {
            if (currentContext.VariableDefined(name))
            {
                currentContext.SetVariable(name, value);
            } else if (globalContext.VariableDefined(name))
            {
                globalContext.SetVariable(name, value);
            } else
            {
                throw new InterpreterException(String.Format("Variable with name {0} is undefined!", name));
            }
        }

        public static void SetCycleContext(bool v)
        {
            currentContext.SetCycleContext(v);
        }

        public static bool isCycleContext()
        {
            return currentContext.isCycleContext();
        }
    }
}
