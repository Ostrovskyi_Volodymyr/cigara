using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cigara
{
    public abstract class Node
    {
        public abstract NodeEvalResult Evaluate();
    }

    public abstract class NExpression : Node
    {
    }

    public abstract class NStatement : Node
    {
    }

    public class NInteger : NExpression
    {
        public int value;

        public NInteger(int val)
        {
            value = val;
        }

        public override NodeEvalResult Evaluate()
        {
            return new NodeEvalResult(value);
        }

        public override string ToString()
        {
            return String.Format("Integer: {0}", value);
        }
    }

    public class NBoolean : NExpression
    {
        public bool value;

        public NBoolean(bool val)
        {
            value = val;
        }

        public override NodeEvalResult Evaluate()
        {
            return new NodeEvalResult(value);
        }

        public override string ToString()
        {
            return String.Format("Boolean: {0}", value);
        }
    }

    public class NChar : NExpression
    {
        public char value;

        public NChar(char val)
        {
            value = val;
        }

        public override NodeEvalResult Evaluate()
        {
            return new NodeEvalResult(value);
        }

        public override string ToString()
        {
            return String.Format("Char: {0}", value);
        }
    }

    public class NIdentifier : NExpression
    {
        public string name;

        public NIdentifier(string name)
        {
            this.name = name;
        }

        public override NodeEvalResult Evaluate()
        {
            return new NodeEvalResult();
        }

        public override string ToString()
        {
            return String.Format("Identifier: {0}", name);
        }
    }

    public class NArray : NExpression
    {
        public List<NExpression> arr;

        public NArray()
        {
            arr = new List<NExpression>();
        }

        public NArray(List<NExpression> arr)
        {
            this.arr = arr;
        }

        public override NodeEvalResult Evaluate()
        {
            List<NodeEvalResult> elems = new List<NodeEvalResult>(arr.Count);
            foreach (var expr in arr)
            {
                elems.Add(expr.Evaluate());
            }
            return new NodeEvalResult(elems);
        }

        public override string ToString()
        {
            string res = "Array: [";
            foreach (var expr in arr)
            {
                res += expr.ToString() + ", ";
            }
            return res + "]";
        }
    }

    public class NString : NExpression
    {
        public string value;

        public NString(string s)
        {
            value = s;
        }

        public override NodeEvalResult Evaluate()
        {
            return new NodeEvalResult(value);
        }

        public override string ToString()
        {
            return String.Format("String: {0}", value);
        }
    }


    public class NVarDecl : NStatement
    {
        public NIdentifier ident;
        public NExpression initValue;

        public NVarDecl(NIdentifier ident, NExpression initValue)
        {
            this.ident = ident;
            this.initValue = initValue;
        }

        public override NodeEvalResult Evaluate()
        {
            Interpreter.AddVariable(ident.name, initValue.Evaluate());
            return new NodeEvalResult();
        }

        public override string ToString()
        {
            return "Var:\n\tID: " + ident + "\n\tValue: " + initValue;
        }
    }

    public class NStmtsBlock : Node
    {
        public List<NStatement> stmts;

        public NStmtsBlock()
        {
            stmts = new List<NStatement>();
        }

        public override NodeEvalResult Evaluate()
        {
            foreach (var stmt in stmts)
            {
                stmt.Evaluate();

                if (Interpreter.GetEvalState() == EvalState.RETURN ||
                    Interpreter.GetEvalState() == EvalState.BREAK)
                    break;
            }
            return new NodeEvalResult();
        }

        public override string ToString()
        {
            string res = "";
            
            foreach (var stmt in stmts)
            {
                res += stmt.ToString() + '\n';
            }
            return res;
        }
    }

    public class NFuncDecl : NStatement
    {
        public NIdentifier ident;
        public List<NIdentifier> paramList;
        public NStmtsBlock stmtsBlock;

        public NFuncDecl(NIdentifier ident, List<NIdentifier> paramList, NStmtsBlock stmtsBlock)
        {
            this.ident = ident;
            this.paramList = paramList;
            this.stmtsBlock = stmtsBlock;
        }

        public override NodeEvalResult Evaluate()
        {
            return new NodeEvalResult();
        }

        public override string ToString()
        {
            string res = "FuncDecl: " + ident + "(";
            foreach (var ident in paramList)
            {
                res += ident + ", ";
            }
            res += ")" + "\nStatements:\n" + stmtsBlock + '\n';
            return res;
        }
    }

    public class NStatementExpr : NStatement
    {
        public NExpression expr;

        public NStatementExpr(NExpression expr)
        {
            this.expr = expr;
        }

        public override NodeEvalResult Evaluate()
        {
            return expr.Evaluate();
        }
        public override string ToString()
        {
            return "StatementExpr: " + expr + '\n';
        }
    }

    public class NCallExpr : NExpression
    {
        public NIdentifier func;
        public List<NExpression> args;

        public NCallExpr(NIdentifier func, List<NExpression> args)
        {
            this.func = func;
            this.args = args;
        }

        public override NodeEvalResult Evaluate()
        {
            NodeEvalResult res = new NodeEvalResult(0);

            NFuncDecl funcdecl = Interpreter.GetFunctionDecl(func.name);
            
            if (funcdecl != null)
            {
                var call_args = new Dictionary<string, NodeEvalResult>();
                foreach (var (param, expr) in funcdecl.paramList.Zip(args))
                {
                    call_args.Add(param.name, expr.Evaluate());
                }

                Interpreter.PushEvalContext();
                Interpreter.PushVarContext();

                foreach (var (param, value) in call_args)
                {
                    Interpreter.AddVariable(param, value);
                }

                funcdecl.stmtsBlock.Evaluate();
                res = Interpreter.GetRetValue();

                Interpreter.PopVarContext();
                Interpreter.PopEvalContext();
            } else
            {
                Interpreter.ExternFunction extfunc = Interpreter.GetExternFunctionDecl(func.name);

                if (extfunc != null)
                {
                    List<NodeEvalResult> arg_list = new List<NodeEvalResult>(args.Count);

                    foreach (var arg in args)
                    {
                        arg_list.Add(arg.Evaluate());
                    }
                    res = extfunc(arg_list);
                } else
                {
                    Console.WriteLine("Function with name {0} is undefined!", func.name);
                }
            }
            return res;
        }

        public override string ToString()
        {
            string res = "CallExpr: " + func + "(";
            foreach (var expr in args)
            {
                res += expr + ", ";
            }
            return res + ")";
        }
    }

    public class NReturnStmt : NStatement
    {
        public NExpression expr;

        public NReturnStmt() {}
        public NReturnStmt(NExpression expr)
        {
            this.expr = expr;
        }

        public override NodeEvalResult Evaluate()
        {
            NodeEvalResult res = new NodeEvalResult(0);
            Interpreter.SetEvalState(EvalState.RETURN);
            if (expr != null)
            {
                res = expr.Evaluate();
                Interpreter.SetRetValue(res);
            }
            return res;
        }

        public override string ToString()
        {
            return "ReturnStmt " + expr;
        }
    }

    public class NVarExpr : NExpression
    {
        public NIdentifier var;

        public NVarExpr(NIdentifier var)
        {
            this.var = var;
        }

        public override NodeEvalResult Evaluate()
        {
            return Interpreter.GetVariable(var.name);
        }

        public override string ToString()
        {
            return "VarExpr: " + var.name;
        }
    }

    public class NVarChangeExpr : NExpression
    {
        public NVarExpr var_expr;
        public NExpression new_value;

        public NVarChangeExpr(NVarExpr var_expr, NExpression new_value)
        {
            this.var_expr = var_expr;
            this.new_value = new_value;
        }

        public override NodeEvalResult Evaluate()
        {
            NodeEvalResult val = new_value.Evaluate();
            Interpreter.SetVariable(var_expr.var.name, val);
            return val;
        }

        public override string ToString()
        {
            return "VarExpr: " + var_expr.var.name;
        }
    }

    public class NIndexExpr : NExpression
    {
        public NExpression arr;
        public NExpression index;

        public NIndexExpr(NExpression arr, NExpression index)
        {
            this.arr = arr;
            this.index = index;
        }

        public override NodeEvalResult Evaluate()
        {
            NodeEvalResult arr_val = arr.Evaluate();
            NodeEvalResult idx_val = index.Evaluate();

            if (idx_val.type == NodeEvalResultType.INTEGER)
            {
                if (arr_val.type == NodeEvalResultType.ARRAY)
                {
                    return arr_val.array_value[idx_val.int_value];
                } else if (arr_val.type == NodeEvalResultType.STRING)
                {
                    return new NodeEvalResult(arr_val.string_value[idx_val.int_value]);
                }
            }

            return new NodeEvalResult(0);
        }

        public override string ToString()
        {
            return "IndexExpr";
        }
    }

    public class NArrayChangeExpr : NExpression
    {
        public NIndexExpr index_expr;
        public NExpression expr;

        public NArrayChangeExpr(NIndexExpr index_expr, NExpression expr)
        {
            this.index_expr = index_expr;
            this.expr = expr;
        }

        public override NodeEvalResult Evaluate()
        {
            NodeEvalResult arr_val = index_expr.arr.Evaluate();
            NodeEvalResult idx_val = index_expr.index.Evaluate();
            NodeEvalResult expr_val = expr.Evaluate();

            if (idx_val.type == NodeEvalResultType.INTEGER)
            {
                if (arr_val.type == NodeEvalResultType.ARRAY)
                {
                    arr_val.array_value[idx_val.int_value] = expr_val;
                    return expr_val;
                } else if (arr_val.type == NodeEvalResultType.STRING &&
                           expr_val.type == NodeEvalResultType.CHAR)
                {
                    StringBuilder strBuilder = new StringBuilder(arr_val.string_value);
                    strBuilder[idx_val.int_value] = expr_val.char_value;
                    arr_val.string_value = strBuilder.ToString();
                    return expr_val;
                }
            }

            return new NodeEvalResult(0);
        }

        public override string ToString()
        {
            return "ArrayChangeExpr";
        }
    }

    public class NBinaryOpExpr : NExpression
    {
        NExpression expr1, expr2;
        delegate NodeEvalResult BinaryOp(NodeEvalResult e1, NodeEvalResult e2);
        BinaryOp op_eval;
        static Dictionary<int, BinaryOp> operators;

        static NBinaryOpExpr()
        {
            operators = new Dictionary<int, BinaryOp>()
            {
                {(int)Tokens.TPLUS, op_plus},
                {(int)Tokens.TMINUS, op_minus},
                {(int)Tokens.TMUL, op_mul},
                {(int)Tokens.TDIV, op_div},
                {(int)Tokens.TLESS, op_less},
                {(int)Tokens.TGREAT, op_great},
                {(int)Tokens.TEQUAL, op_equal},
                {(int)Tokens.TNEQUAL, op_nequal},
            };
        }

        public NBinaryOpExpr(NExpression expr1, int op, NExpression expr2)
        {
            this.expr1 = expr1;
            this.expr2 = expr2;
            op_eval = operators[op];
        }

        public override NodeEvalResult Evaluate()
        {
            NodeEvalResult e1 = expr1.Evaluate();
            NodeEvalResult e2 = expr2.Evaluate();
            return op_eval(e1, e2);
        }

        public override string ToString()
        {
            return "BinaryOpExpr";
        }

        static NodeEvalResult op_plus(NodeEvalResult e1, NodeEvalResult e2)
        {
            if (e1.type == e2.type && e1.type == NodeEvalResultType.INTEGER)
            {
                return new NodeEvalResult(e1.int_value + e2.int_value);
            } else if (e1.type == NodeEvalResultType.ARRAY)
            {
                List<NodeEvalResult> new_arr = new List<NodeEvalResult>(e1.array_value);
                new_arr.Add(e2);
                return new NodeEvalResult(new_arr);
            } else if (e2.type == NodeEvalResultType.ARRAY)
            {
                List<NodeEvalResult> new_arr = new List<NodeEvalResult>(e2.array_value);
                new_arr.Insert(0, e1);
                return new NodeEvalResult(new_arr);
            } else if (e1.type == NodeEvalResultType.STRING && e2.type == NodeEvalResultType.CHAR)
            {
                return new NodeEvalResult(e1.string_value + e2.char_value);
            } else if (e2.type == NodeEvalResultType.STRING && e1.type == NodeEvalResultType.CHAR)
            {
                return new NodeEvalResult(e1.char_value + e2.string_value);
            }
                return new NodeEvalResult(0);
        }

        static NodeEvalResult op_minus(NodeEvalResult e1, NodeEvalResult e2)
        {
            if (e1.type == e2.type && e1.type == NodeEvalResultType.INTEGER)
            {
                return new NodeEvalResult(e1.int_value - e2.int_value);
            }
            return new NodeEvalResult(0);
        }

        static NodeEvalResult op_mul(NodeEvalResult e1, NodeEvalResult e2)
        {
            if (e1.type == e2.type && e1.type == NodeEvalResultType.INTEGER)
            {
                return new NodeEvalResult(e1.int_value * e2.int_value);
            }
            return new NodeEvalResult(0);
        }

        static NodeEvalResult op_div(NodeEvalResult e1, NodeEvalResult e2)
        {
            if (e1.type == e2.type && e1.type == NodeEvalResultType.INTEGER)
            {
                if (e2.int_value != 0)
                {
                    return new NodeEvalResult(e1.int_value / e2.int_value);
                }
            }
            return new NodeEvalResult(0);
        }

        static NodeEvalResult op_less(NodeEvalResult e1, NodeEvalResult e2)
        {
            return e1 < e2;
        }

        static NodeEvalResult op_great(NodeEvalResult e1, NodeEvalResult e2)
        {
            return e1 > e2;
        }

        static NodeEvalResult op_equal(NodeEvalResult e1, NodeEvalResult e2)
        {
            return e1 == e2;
        }

        static NodeEvalResult op_nequal(NodeEvalResult e1, NodeEvalResult e2)
        {
            return e1 != e2;
        }
    }

    public class NIfStmt : NStatement
    {
        public NExpression expr;
        public NStmtsBlock if_block;
        public NStmtsBlock else_block;

        public NIfStmt(NExpression expr, NStmtsBlock if_block)
        {
            this.expr = expr;
            this.if_block = if_block;
        }

        public NIfStmt(NExpression expr, NStmtsBlock if_block, NStmtsBlock else_block)
        {
            this.expr = expr;
            this.if_block = if_block;
            this.else_block = else_block;
        }

        public override NodeEvalResult Evaluate()
        {
            if (expr.Evaluate().toBoolean())
            {
                Interpreter.PushVarContext();
                if_block.Evaluate();
                Interpreter.PopVarContext();
            } else if (else_block != null)
            {
                Interpreter.PushVarContext();
                else_block.Evaluate();
                Interpreter.PopVarContext();
            }
            return new NodeEvalResult(0);
        }

        public override string ToString()
        {
            return "IfStmt";
        }
    }

    public class NWhileStmt : NStatement
    {
        public NExpression cond;
        public NStmtsBlock stmts_block;

        public NWhileStmt(NExpression cond, NStmtsBlock stmts_block)
        {
            this.cond = cond;
            this.stmts_block = stmts_block;
        }

        public override NodeEvalResult Evaluate()
        {
            while (cond.Evaluate().toBoolean())
            {
                Interpreter.PushVarContext();
                Interpreter.SetCycleContext(true);
                stmts_block.Evaluate();
                Interpreter.PopVarContext();

                if (Interpreter.GetEvalState() == EvalState.RETURN ||
                    Interpreter.GetEvalState() == EvalState.BREAK)
                    break;
            }
            if (Interpreter.GetEvalState() == EvalState.BREAK)
            {
                Interpreter.SetEvalState(EvalState.CONTINUE);
            }
            return new NodeEvalResult(0);
        }

        public override string ToString()
        {
            return "WhileStmt(" + cond + ") Statements:\n" + stmts_block;
        }
    }

    class NBreakStmt : NStatement
    {
        public override NodeEvalResult Evaluate()
        {
            if (Interpreter.isCycleContext())
            {
                Interpreter.SetEvalState(EvalState.BREAK);
            } else
            {
                throw new InterpreterException("Break error: Not cycle context");
            }
            return new NodeEvalResult(0);
        }

        public override string ToString()
        {
            return "ReturnStmt";
        }
    }
}
